﻿module EventStore =
    type Aggregate = System.Guid

    type EventProducer<'Event> = 'Event list -> 'Event list

    type EventStore<'Event> = {
        Get: unit -> Map<Aggregate,'Event list>
        GetStream: Aggregate -> 'Event list
        Append: Aggregate -> 'Event list -> unit
        Evolve: Aggregate -> EventProducer<'Event> -> unit
    }
        
    // What the MailboxProcessor does
    type Message<'Event> = 
        | Append of Aggregate * 'Event list
        | GetStream of Aggregate * AsyncReplyChannel<'Event list>
        | Get of AsyncReplyChannel<Map<Aggregate,'Event list>> // What kind of reply to expect
        | Evolve of Aggregate * EventProducer<'Event>

    let getEventsForAggregate aggregate eventHistory = 
        eventHistory
        |> Map.tryFind aggregate
        |> Option.defaultValue []

    let Initialize() : EventStore<'Event> = 
        let agent = 
            MailboxProcessor.Start(fun inbox ->
                let rec loop eventHistory = 
                    async {
                        let! message = inbox.Receive()
                        match message with 
                        | Append (aggregate, events) -> 
                            let stream = eventHistory |> getEventsForAggregate aggregate
                            let newEventHistory = eventHistory |> Map.add aggregate (stream @ events)
                            return! loop newEventHistory

                        | Get reply -> 
                            reply.Reply eventHistory
                            return! loop eventHistory

                        | GetStream (aggregate, reply) ->
                            reply.Reply (eventHistory |> getEventsForAggregate aggregate)
                            return! loop eventHistory

                        | Evolve (aggregate, eventProducer) ->
                            let stream = eventHistory |> getEventsForAggregate aggregate
                            let newEvents = eventProducer stream
                            let newEventHistory = eventHistory |> Map.add aggregate (stream @ newEvents)
                            return! loop newEventHistory
                    } 
                loop Map.empty
            )
        let appendEvents aggregate events = agent.Post (Append (aggregate, events))
        let getEvents () = agent.PostAndReply Get
        let getStream aggregate = agent.PostAndReply (fun reply -> GetStream (aggregate, reply))
        let evolveEventsHistory aggregate eventProducer = agent.Post (Evolve (aggregate, eventProducer))

        { 
            Append = appendEvents
            Get = getEvents
            GetStream = getStream
            Evolve = evolveEventsHistory 
        }

module Domain = 
    type Drink = 
        | Coca
        | Pepsi
        | Sprite
        | Water

    type Event =
        | DrinkSold of Drink
        | DrinkRestocked of Drink * int
        | DrinkWentOutOfStock of Drink
        | DrinkWasNotInStock of Drink

module Projections = 
    open Domain
    type Projection<'State,'Event> = {
        Init: 'State
        Update: 'State -> 'Event -> 'State
    }

    let project (projection: Projection<_,_>) events = 
        events |> List.fold projection.Update projection.Init

    let soldOfDrink drink state = 
        state
        |> Map.tryFind drink
        |> Option.defaultValue 0

    let updateSoldDrinks state event = 
        match event with
        | DrinkSold drink ->
            state 
            |> soldOfDrink drink
            |> fun bottles -> state |> Map.add drink (bottles + 1)
        | _ -> state

    let restock drink numberOfBottles stock = 
        stock
        |> Map.tryFind drink
        |> Option.defaultValue 0
        |> fun bottles -> stock |> Map.add drink (bottles + numberOfBottles)
    
    let updateDrinksInStock stock event =
        match event with
        | DrinkSold drink -> 
            stock |> restock drink -1
        | DrinkRestocked (drink, numberOfBottles) -> 
            stock |> restock drink numberOfBottles
        | _ -> stock

    let soldDrinks: Projection<Map<Drink,int>, Event> = 
        {
            Init = Map.empty
            Update = updateSoldDrinks
        }

    let drinksInStock: Projection<Map<Drink,int>, Event> =
        {
            Init = Map.empty
            Update = updateDrinksInStock
        }
    
    let stockOf drink stock = 
        stock
        |> Map.tryFind drink
        |> Option.defaultValue 0 

module Behavior =
    open Domain
    open Projections

    let sellDrink drink (events: Event list) =
        let stock = 
            events
            |> project drinksInStock
            |> stockOf drink

        match stock with 
        | 0 -> [DrinkWasNotInStock drink]
        | 1 -> [DrinkSold drink; DrinkWentOutOfStock drink]
        | _ -> [DrinkSold drink]

    let restockDrink drink numberOfBottles (events: Event list) =
        [DrinkRestocked (drink, numberOfBottles)]

module Tests = 
    open Expecto
    open Expecto.Expect
    open Domain

    let Given = id
    let When eventProducer events = 
        eventProducer events
    let Then expectedEvents actualEvents = 
        equal actualEvents expectedEvents "actual events should equal to the expected events"

    let tests = 
        testList "sellDrinks"
            [
                test "DrinkSold" {
                    Given [ DrinkRestocked (Pepsi, 2) ]
                    |> When Behavior.sellDrink Pepsi
                    |> Then [ DrinkSold Pepsi ]
                }
                test "DrinkSold, DrinkWentOutOfStock" {
                    Given [ 
                        DrinkRestocked (Pepsi, 3)
                        DrinkSold Pepsi
                        DrinkSold Pepsi
                    ]
                    |> When Behavior.sellDrink Pepsi
                    |> Then [ 
                        DrinkSold Pepsi
                        DrinkWentOutOfStock Pepsi
                    ]
                }
                test "DrinkWasNotInStock" {
                    Given [ 
                        DrinkRestocked (Pepsi, 3)
                        DrinkSold Pepsi
                        DrinkSold Pepsi
                        DrinkSold Pepsi
                    ]
                    |> When Behavior.sellDrink Pepsi
                    |> Then [ DrinkWasNotInStock Pepsi ]
                }
            ]

module Helper =
    open Projections
    open Expecto

    let printUnorderedList list = 
        list |> List.iteri (fun index item -> printfn "%i)  %A" (index+1) item)

    let printEvents header events =
        events
        |> List.length
        |> printfn "History for %s (Lenth: %i)" header
        events |> printUnorderedList
     
    let printSoldOfDrink drink state = 
        state
        |> soldOfDrink drink
        |> printfn "Sold %A: %i" drink
     
    let printTotalEventsHistory eventHistory = 
        eventHistory
        |> Map.fold (fun length _ events -> length + (events |> List.length)) 0
        |> printfn "Total History Length %i"

    let runTests () =
        runTests defaultConfig Tests.tests |> ignore

open EventStore
open Domain
open Projections
open Helper

[<EntryPoint>]
let main _ =

    runTests()
    
    let eventStore: EventStore<Event> = EventStore.Initialize()

    let vendingMachine1 = System.Guid.NewGuid()
    let vendingMachine2 = System.Guid.NewGuid()

    eventStore.Evolve vendingMachine1 (Behavior.sellDrink Pepsi)
    eventStore.Evolve vendingMachine1 (Behavior.sellDrink Coca)
    eventStore.Evolve vendingMachine1 (Behavior.restockDrink Coca 4)
    eventStore.Evolve vendingMachine1 (Behavior.sellDrink Coca)
    eventStore.Evolve vendingMachine2 (Behavior.restockDrink Water 2)
    eventStore.Evolve vendingMachine2 (Behavior.sellDrink Water)

    let vendingMachine1Events = eventStore.GetStream vendingMachine1
    let vendingMachine2Events = eventStore.GetStream vendingMachine2

    vendingMachine1Events |> printEvents " Machine 1"
    vendingMachine2Events |> printEvents " Machine 2"

    let events = eventStore.Get()

    let sold: Map<Drink, int> =
        vendingMachine1Events |> project soldDrinks

    printSoldOfDrink Pepsi sold
    printSoldOfDrink Coca sold


    0
